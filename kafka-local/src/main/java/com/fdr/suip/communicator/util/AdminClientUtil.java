//package com.demo.kafka.local.util;
//
//import lombok.extern.slf4j.Slf4j;
//import org.apache.kafka.clients.CommonClientConfigs;
//import org.apache.kafka.clients.admin.AdminClient;
//import org.apache.kafka.clients.admin.AdminClientConfig;
//import org.apache.kafka.clients.admin.DescribeConsumerGroupsResult;
//import org.apache.kafka.clients.admin.KafkaAdminClient;
//import org.apache.kafka.clients.admin.ListConsumerGroupsResult;
//import org.apache.kafka.clients.admin.TopicListing;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.autoconfigure.kafka.KafkaProperties;
//import org.springframework.retry.annotation.Backoff;
//import org.springframework.retry.annotation.Retryable;
//import org.springframework.scheduling.annotation.Scheduled;
//import org.springframework.stereotype.Component;
//
//import javax.annotation.PostConstruct;
//import java.util.Arrays;
//import java.util.Properties;
//import java.util.concurrent.ExecutionException;
//
//@Component
//@Slf4j(topic = "AdminClientUtil Logger")
////@CommonsLog(topic = "AdminClientUtil Logger")
//public class AdminClientUtil {
//
//    @Autowired
//    private KafkaProperties kafkaProperties;
//
//    private AdminClient kafkaAdminClient;
//
//    private Properties adminProperties = new Properties();
//
//    @PostConstruct
//    public void initialize(){
//        log.info("Initializing AdminClientUtil");
//
//        adminProperties.put(CommonClientConfigs.BOOTSTRAP_SERVERS_CONFIG,kafkaProperties.getBootstrapServers().get(0));
//    }
//
//    @Scheduled(cron = "0 */1 * * * *")
//    @Retryable(value = { Exception.class }, backoff = @Backoff(delay = 2000))
//    public void checkConsumerConnection() throws Exception{
//
//
//        kafkaAdminClient = KafkaAdminClient.create(adminProperties);
//
//        ListConsumerGroupsResult groups =  kafkaAdminClient.listConsumerGroups();
//        log.info("Groups {}",groups.toString());
//        DescribeConsumerGroupsResult groupsResult = kafkaAdminClient.describeConsumerGroups(Arrays.asList("suip_consumer_group"));
//        log.info("Group result {}",groupsResult);
//        log.info("{}",groupsResult.describedGroups());
//        groupsResult.describedGroups().keySet().forEach(key ->{
//            log.info("******Group Result {}",groupsResult.describedGroups().get(key));
//        });
//        try {
//            for (TopicListing topicListing : kafkaAdminClient.listTopics().listings().get()) {
//                log.info("Topic ",topicListing);
//            }
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        } catch (ExecutionException e) {
//            e.printStackTrace();
//        }
//    }
//
//}
