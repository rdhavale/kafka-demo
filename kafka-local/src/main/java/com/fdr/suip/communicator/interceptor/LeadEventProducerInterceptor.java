package com.fdr.suip.communicator.interceptor;

import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.ProducerInterceptor;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
@Slf4j(topic = "LeadEventProducerInterceptor logger")
public class LeadEventProducerInterceptor implements ProducerInterceptor {
    @Override
    public ProducerRecord onSend(ProducerRecord record) {
        return record;
    }

    @Override
    public void onAcknowledgement(RecordMetadata metadata, Exception exception) {
        log.info(String.format("onAcknowledgement topic=%s, part=%d, offset=%d",
                metadata.topic(), metadata.partition(), metadata.offset()));
    }

    @Override
    public void close() {
        log.info("Producer is closed");
    }

    @Override
    public void configure(Map<String, ?> configs) {
        log.info("configuring");
    }
}
