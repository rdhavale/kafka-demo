package com.fdr.suip.communicator.service;

import lombok.extern.apachecommons.CommonsLog;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
@CommonsLog(topic = "Consumer Logger")
public class Consumer {

    @Value("${topic.name}")
    private String TOPIC;

    @KafkaListener(topics = "suip.lead.event", groupId = "group_id")
    public void consume(ConsumerRecord<String, Object> record) throws IOException {
        log.info(String.format("#### -> Consumed message -> %s", record.value()));
    }
}
