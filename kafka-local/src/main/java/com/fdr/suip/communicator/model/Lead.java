package com.fdr.suip.communicator.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;


@Getter
@Setter
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Lead implements Serializable {

    private static final long serialVersionUID = 1L;

	@JsonProperty(value = "attributes")
	private Attributes attributes;

    @JsonProperty(value = "Id")
    private String id;

    @JsonProperty(value = "Name")
    private String name;

    @JsonProperty(value = "Email")
    private String email;

    @JsonProperty(value = "Client_Email__c")
    private String clientEmail;

    @JsonProperty(value = "Phone")
    private String phone;

    @JsonProperty(value = "MobilePhone")
    private String mobilePhone;

    @JsonProperty(value = "Evening_Phone__c")
    private String eveningPhone;

    @JsonProperty(value = "Address")
    private String address;

    @JsonProperty(value = "Status")
    private String status;

    @JsonProperty(value = "Sub_Status__c")
    private String subStatus;

    @JsonProperty(value = "type")
    private String type;

    @JsonProperty(value = "OwnerId")
    private String owner;

    @JsonProperty(value = "Owner")
    private Owner ownerDetails;

    @JsonProperty(value = "CreatedDate")
    private String createdDate;

    @JsonProperty(value = "Phone__c")
    private String opportunityPhone;

    @JsonProperty(value = "Mobile_Phone__c")
    private String opportunityMobile;

    @JsonProperty(value = "StageName")
    private String opportunityStatus;

    @JsonProperty(value = "Current_UW__c")
    private String currentUW;

    @JsonProperty(value = "Original_UW__c")
    private String originalUW;

    @JsonProperty(value = "Current_ARA__c")
    private String currentARA;

    @JsonProperty(value = "Original_ARA__c")
    private String originalARA;

    @JsonProperty(value = "Current_ESA__c")
    private String currentESA;

    @JsonProperty(value = "Original_ESA__c")
    private String originalESA;

    @JsonProperty(value = "Current_WCA__c")
    private String currentWCA;

    @JsonProperty(value = "Original_WCA__c")
    private String originalWCA;

    @JsonProperty(value = "NumberToDial")
    private String numberToDial;

    @JsonProperty(value = "State")
    private String state;

    @JsonProperty(value = "Client_Physical_State__c")
    private String clientPhysicalState;

    @JsonProperty(value = "Estimated_Total_Debt__c")
    private String estimatedDebt;

    @JsonProperty(value = "FDR_Applicant_Id__c")
    private String fdrApplicantId;

    @JsonProperty(value = "Lead_Type__c")
    private String leadType;
    
    @JsonProperty(value = "Last_Contacted_Date__c")
    private String lastContactedDate;
    
    @JsonProperty(value = "Eligible_for_CS__c")
    private boolean eligibleForCS;
    
    @JsonProperty(value = "Lead_State__c")
    private String leadState;
    
    @JsonProperty(value = "Inactive_Reason__c")
    private String inactiveReason;
    
    @JsonProperty(value = "Lead_Partner__c")
    private String leadPartner;
    
    @JsonProperty(value = "Contacted_Days__c")
    private String contactedDays;
    
    private String leadEventType;

    @JsonProperty(value = "Voicemail_left__c")
    private boolean voiceMailIndicator;
   
    @JsonProperty(value = "Owner_Email__c")//Used in file task
    private String ownerEmail;

    @JsonProperty(value = "User")
    private User user;

    @JsonProperty(value = "LastModifiedDate")
    private String lastModifiedDate;

    @JsonProperty(value = "Lead_Platform_Id__c")
    private String leadPlatformId;

    @JsonProperty(value = "LastModifiedById")
    private String lastModifiedById;

    @JsonProperty(value = "First_Name__c")
    private String firstName;

    @JsonProperty(value = "Last_Name__c")
    private String lastName;

    @JsonProperty(value = "State_TZ_Offset__c")
    private int timeZoneOffset;

    @JsonProperty(value = "Co_App_FDR_Applicant_Id__c")
    private String coAppFdrApplicantId;

    @JsonProperty(value = "Co_App_CBR_Id__c")
    private String coAppCbrId;

    @JsonProperty(value = "Co_App_Credit_Pull_counter__c")
    private Integer creditPullCounter;

    @JsonProperty(value = "CA_CP_Is_Scss__c")
    private boolean creditPullSuccess;

    //To mark event as Co-app Credit pull event
    @JsonProperty(value = "isCreditPullEvent")
    private boolean isCreditPullEvent;

    //To avoid sending another file task event when a co-app credit pull event is sent
    @JsonProperty(value = "isEventForFileTask")
    private boolean isEventForFileTask;

    @JsonProperty(value = "taskTimeOut")
    private int taskTimeOut;
}
