package com.fdr.suip.communicator.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fdr.suip.communicator.model.LeadEvent;
import com.fdr.suip.communicator.service.Producer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/event")
public class KafkaController {

    @Autowired
    private Producer producer;

    @PostMapping(value = "/publish")
    public void sendMessageToKafkaTopic(@RequestParam("name") String name, @RequestParam("age") Integer age) {
//        producer.sendMessage(new User(name, age));
    }

    @PostMapping(value = "/publish/lead")
    public void sendMessageToKafkaTopic(@RequestBody LeadEvent leadEvent) throws JsonProcessingException {
        producer.sendMessage(leadEvent);
    }
}
