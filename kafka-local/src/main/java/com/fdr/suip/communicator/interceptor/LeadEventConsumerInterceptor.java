package com.fdr.suip.communicator.interceptor;

import com.fdr.suip.communicator.model.LeadEvent;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerInterceptor;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.OffsetAndMetadata;
import org.apache.kafka.common.TopicPartition;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
@Slf4j(topic = "LeadEventConsumerInterceptor logger")
public class LeadEventConsumerInterceptor implements ConsumerInterceptor<String, LeadEvent> {

    @Override
    public ConsumerRecords<String, LeadEvent> onConsume(ConsumerRecords<String, LeadEvent> records) {
        log.info("Messages received {}",records.count());
        return records;
    }

    @Override
    public void onCommit(Map<TopicPartition, OffsetAndMetadata> offsets) {
        log.info("Message commit {}",offsets.keySet());
    }

    @Override
    public void close() {
        log.info("Consumer is closed");
    }

    @Override
    public void configure(Map<String, ?> configs) {
        log.info("configure..");
    }
}
