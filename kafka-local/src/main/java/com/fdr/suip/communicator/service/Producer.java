package com.fdr.suip.communicator.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fdr.suip.communicator.model.LeadEvent;
import lombok.extern.apachecommons.CommonsLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
@CommonsLog(topic = "Producer Logger")
public class Producer {

    @Value("${topic.name}")
    private String TOPIC;

    @Autowired
    private KafkaTemplate<String, Object> kafkaTemplate;

    @Autowired
    private ObjectMapper objectMapper;

    public void sendMessage(LeadEvent leadEvent) throws JsonProcessingException {
        String leadEventStr = objectMapper.writeValueAsString(leadEvent);
        log.info(leadEventStr);

        this.kafkaTemplate.send(TOPIC, String.valueOf(leadEvent.getEventMetadata().getReplayId()),leadEventStr);

        log.info("#### -> Produced event -> {}");
        log.info(leadEventStr);
    }

    /*public void sendMessage(User user) {
        this.kafkaTemplate.send(TOPIC, user.getName(),user);
        log.info(String.format("#### -> Produced user -> %s", user));
    }*/
}
