package com.fdr.suip.communicator.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Getter
@Setter
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class EventMetadata implements Serializable {

    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "createdDate")
    private String createdDate;

    @JsonProperty(value = "type")
    private String type;

    @JsonProperty(value = "replayId")
    private long replayId;
}
